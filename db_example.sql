--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Debian 15.3-0+deb12u1)
-- Dumped by pg_dump version 15.3 (Debian 15.3-0+deb12u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: notes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notes (
    id integer NOT NULL,
    content text,
    created_at timestamp with time zone DEFAULT now(),
    title character varying(255),
    tags character varying(255)[]
);


ALTER TABLE public.notes OWNER TO postgres;

--
-- Name: notes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notes_id_seq OWNER TO postgres;

--
-- Name: notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notes_id_seq OWNED BY public.notes.id;


--
-- Name: notes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notes ALTER COLUMN id SET DEFAULT nextval('public.notes_id_seq'::regclass);


--
-- Data for Name: notes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notes (id, content, created_at, title, tags) FROM stdin;
13	<blockquote><p><em>Codez de manière à ce que le prochain programmeur qui lira votre code vous remercie</em></p><p><strong>Martin Fowler</strong></p></blockquote>	2023-06-29 14:39:53.797166+02	Martin Fowler	{citation,prog,inspirant}
12	<blockquote><p><em>Le meilleur moment pour planter un arbre était il y a 20 ans. Le deuxième meilleur moment est maintenant</em><br><strong><br>Proverbe chinois</strong></p></blockquote>	2023-06-29 14:32:55.487217+02	Le temps	{proverbe,proverbe-chinois}
10	<ol><li><p>Paris</p></li><li><p>Grenoble</p></li><li><p>Lyon</p></li><li><p>Marseille</p></li><li><p>Nantes</p></li><li><p>Strasbourg</p></li></ol>	2023-06-29 14:29:54.612775+02	Visites	{todo}
11	<blockquote><p><em>La seule façon de faire du bon travail est d'aimer ce que vous faites</em></p><p><strong>Steve Jobs</strong></p></blockquote>	2023-06-29 14:31:25.239029+02	Steve Jobs	{citation}
9	<pre><code># Generate a random number\nimport random</code></pre><pre><code># Define a list of colors\ncolors = ['red', 'blue', 'green', 'yellow']</code></pre><pre><code># Select a random color\nrandom_color = random.choice(colors)</code></pre><pre><code># Print the random color\nprint("The random color is:", random_color)</code></pre>	2023-06-29 14:26:01.856812+02	Python	{code}
7	<h1>A acheter</h1><ul><li><p>Pommes</p></li></ul><ul><li><p>Bananes</p></li><li><p>Oranges</p></li><li><p>Lait</p></li><li><p>Pain</p></li><li><p>Œufs</p></li><li><p>Fromage</p></li></ul><p></p>	2023-06-29 13:01:03.475875+02	Courses	{todo}
2	<pre><code>\n-- Create a table\nCREATE TABLE Customers (  id INT PRIMARY KEY,  name VARCHAR(50),  email VARCHAR(50));\n</code></pre><pre><code>-- Insert data into the table\nINSERT INTO Customers (id, name, email) VALUES (1, 'John Doe', 'john@example.com');</code></pre><pre><code>-- Retrieve data from the table\nSELECT * FROM Customers;</code></pre>	2023-06-29 11:39:31.744031+02	SQL	{code}
52	<h1>Cette application de notes utilise tiptap</h1>	2023-06-30 17:49:10.463972+02	Tiptap	{tiptap}
\.


--
-- Name: notes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notes_id_seq', 52, true);


--
-- Name: notes notes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT notes_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

