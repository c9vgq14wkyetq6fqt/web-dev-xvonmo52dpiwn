import { createRouter, createWebHistory } from 'vue-router'
import NotesList from '../views/NotesList.vue'

const routes = [
  {
    path: '/',
    name: 'NotesList',
    component: NotesList
  },
  {
    path: '/managenote/:id',
    name: 'ManageNote',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "managenote" */ '../views/ManageNote.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
