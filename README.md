# Notes App

## Description
L'application proposé permet de gérer ses notes : création, édition, visualisation, suppression. L'éditeur de note inclut plusieurs fonctionnalités stylistiques et de formatage (bullet list, bold, italic ...). L'application permet aussi d'associer des tags aux notes, et de faire un "tag filtering" sur les notes pour sélectionner celles dont on a besoin. 

## Technologies utilisées
- NodeJS
- Frontend : Vue 3, axios pour consommer l'API REST, tiptap pour l'éditeur de notes
- Backend  : Express pour la construction de l'API REST

## Mise en place et installation
1. Installez PostgreSQL (version utilisé pour le développement : 15.3). Assurez-vous que le serveur est en marche.<br />
Installez également la dernière version de NodeJS (avec npm et npx)
2. Créez une base de données, puis importez-y le fichier db_example.sql
3. Changez les informations de connexion à la BD dans le fichier app.js pour les faire correspondre à votre configuration. Changez éventuellement le port d'écoute du serveur.
3. Depuis la racine du projet, lancez le backend puis le frontend par les commandes suivantes :

```shell
cd backend && npm i && node app.js
```
```shell
cd frontend && npm i && npm run serve
```