const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { Pool } = require('pg');

const app = express();
const port = 3000;

// Configuration du middleware CORS
app.use(cors());

// Configuration de body-parser pour la gestion des données JSON
app.use(bodyParser.json());

// Configuration de la connexion à la base de données PostgreSQL
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'notes_db',
    password: 'postgres',
    port: 5432,
  });

// Route GET pour récupérer toutes les notes
app.get('/api/notes', (req, res) => {
  pool.query('SELECT * FROM notes', (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Erreur lors de la récupération des notes' });
    } else {
      res.status(200).json(results.rows);
    }
  });
});

// Route GET pour obtenir une note par son ID
app.get('/api/notes/:id', (req, res) => {
  const noteId = req.params.id;

  pool.query('SELECT * FROM notes WHERE id = $1', [noteId], (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Erreur lors de la récupération des notes' });
    } else {
      if (results.rowCount === 0) {
        res.status(404).json({ error: 'Ressource non trouvée' });
      } else {
        res.status(200).json(results.rows[0]);
      }
    }
  });
});

// Route POST pour créer une nouvelle note
app.post('/api/notes', (req, res) => {
  const { content, title, tags } = req.body;

  pool.query('INSERT INTO notes (content, title, tags) VALUES ($1, $2, $3) RETURNING *', [content, title, tags], (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Erreur lors de la création de la note' });
    } else {
      res.status(201).json({ success_msg: 'note added' });
    }
  });
});

// Route PUT pour mettre à jour une note existante
app.put('/api/notes/:noteId', (req, res) => {
  const noteId = req.params.noteId;
  const { content, title, tags } = req.body;

  pool.query('UPDATE notes SET content = $1, title = $2, tags = $3 WHERE id = $4 RETURNING *', [content, title, tags, noteId], (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Erreur lors de la mise à jour de la note' });
    } else {
      if (results.rowCount === 0) {
        res.status(404).json({ error: 'Note non trouvée' });
      } else {
        res.status(200).json(results.rows[0]);
      }
    }
  });
});

// Route DELETE pour supprimer une note par son ID
app.delete('/api/notes/:id', (req, res) => {
  const noteId = req.params.id;

  pool.query('DELETE FROM notes WHERE id = $1', [noteId], (error) => {
    if (error) {
      console.error(error);
      res.status(500).json({ error: 'Erreur lors de la suppression de la note' });
    } else {
      res.sendStatus(204);
    }
  });
});

app.listen(port, () => {
  console.log(`Serveur backend écoutant sur le port ${port}`);
});
